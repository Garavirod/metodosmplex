var VarIterablesCol = new Array();
var VarIterablesRow = new Array();

// Crea la estructura de la tabla con sus respectivas cajas de texto
function buildTable(V, R) {
    var restricciones = R;
    var variables = V;
    var padre = document.getElementById('datos');

    // Estructura de la tabla
    var tabla = document.createElement('table'); //Tabla
    var cabecera = document.createElement("thead"); //Cabecera de la tabla
    var cuerpo_tabla = document.createElement("tbody"); //cuerpo de la tabla
    var celda;
    var contenido;
    var filas;

    tabla.setAttribute("class", "table table-hover");
    tabla.setAttribute("id", "tabla");
    celda = document.createElement("th");
    contenido = document.createTextNode("Num Ecuaciones");
    celda.appendChild(contenido);
    tabla.appendChild(celda);

    // Cabecera
    for (var i = 1; i <= variables; i++) {
        celda = document.createElement("th");
        contenido = document.createTextNode("X" + i);
        celda.appendChild(contenido);
        tabla.appendChild(celda);
        VarIterablesCol.push("X" + i);
    }

    for (var i = 1; i <= restricciones; i++) {
        celda = document.createElement("th");
        contenido = document.createTextNode("S" + i);
        celda.appendChild(contenido);
        tabla.appendChild(celda);
        VarIterablesCol.push("S" + i);

    }

    // Solucion
    celda = document.createElement("th");
    contenido = document.createTextNode("Solución");
    celda.appendChild(contenido);
    tabla.appendChild(celda);

    // Funcion objetivo
    var tam = 1 + variables + restricciones;
    filas = document.createElement("tr");
    filas.setAttribute("id", "restriccion0");
    for (var i = 1; i <= tam; i++) {
        if (i == 1) {
            celda = document.createElement("th");
            contenido = document.createTextNode("Z");
        }
        else if (i <= variables + 1 && i > 1) {
            celda = document.createElement("td");
            contenido = document.createElement("input");
            contenido.setAttribute("name", "variables_restriccion_0");
        }
        else {
            celda = document.createElement("td");
            contenido = document.createElement("input");
            contenido.setAttribute("name", "variables_restriccion_0");
            contenido.setAttribute("value", "0");
            contenido.setAttribute("disabled", true);
        }
        celda.appendChild(contenido);
        filas.appendChild(celda);
    }
    cuerpo_tabla.appendChild(filas);
    tabla.appendChild(cuerpo_tabla);


    // Num de restricciones
    for (var i = 1; i <= restricciones; i++) {
        filas = document.createElement("tr");
        filas.setAttribute("id", "restriccion" + i);
        inputs = document.createElement("td");
        contenido = document.createTextNode("S" + i);
        VarIterablesRow.push("S" + i);
        celda = document.createElement("th");
        celda.setAttribute("id", "R" + i);
        celda.appendChild(contenido);
        filas.appendChild(celda);
        // Inputs para las variables de entrada
        for (var j = 1; j <= variables; j++) {
            celda = document.createElement("td");
            contenido = document.createElement("input");
            contenido.setAttribute("name", "variables_restriccion_" + (i));
            celda.appendChild(contenido);
            filas.appendChild(celda);
        }

        cuerpo_tabla.appendChild(filas);
        tabla.appendChild(cuerpo_tabla);
    }
    padre.appendChild(tabla);
    // Hacemos visible el boton de resolver
    $('#calculate').removeClass("solve_system_hide");
    var row;
    for (var j = 1; j <= restricciones; j++) {
        row = document.getElementById('restriccion' + j);
        for (var i = 1; i <= restricciones; i++) {
            celda = document.createElement("td");
            contenido = document.createElement("input");
            if (i == j)
                contenido.setAttribute("value", "1");
            else
                contenido.setAttribute("value", "0");

            contenido.setAttribute("disabled", true);
            contenido.setAttribute("name", "variables_restriccion_" + j);
            celda.appendChild(contenido);
            row.appendChild(celda);
        }
    }

    // Vector de Soluciones
    for (var j = 0; j <= restricciones; j++) {
        row = document.getElementById('restriccion' + j);
        celda = document.createElement("td");
        contenido = document.createElement("input");
        contenido.setAttribute("name", "variables_restriccion_" + j);
        if (j == 0) {
            contenido.setAttribute("disabled", true);
            contenido.setAttribute("value", '0');
        }
        celda.appendChild(contenido);
        row.appendChild(celda);
    }
}

function buildChangeVector(restricciones) {
    for (var i = 0; i < restricciones; i++)
        $("#new_vectors").append("<input class='form-control form-control-md m-2 text-center' name='new_VB' id='VB' type='text' placeholder='B" + (i + 1) + "'>");
    $("#new_vectorA").append("<input class='form-control form-control-md m-2 text-center VA' name='new_VA' id='VA" + (i) + "'" + "type='text' placeholder='A" + (i) + "'>");
}

// Retorna un arreglo con los valeres extraidos de los inputs.
function extractValues(elems) {
    var new_array = new Array();
    for (var i = 0; i < elems.length; i++)
        new_array.push(parseInt(elems[i].value));
    return new_array;
}

// ----------------------CODIGO METODO SIMPLEX------------------
// Suma la fila pivote con un renglon de la matriz
function Opera(filaPivote, renglon, CP) {
    var new_row = new Array(renglon.length);
    for (var i = 0; i < renglon.length; i++) {
        var num = renglon[CP];
        num *= -1;
        new_row[i] = (num * filaPivote[i]) + (renglon[i]);
    }
    return new_row;
}

function Gauss(matrix, CP, FP, PP) {
    // hacemos 1's punto pivote
    for (var i = 0; i < matrix[FP].length; i++)
        matrix[FP][i] = matrix[FP][i] / PP;

    // Hacemos 0's los elementos arriba y abajo del punto pivote
    for (var i = 0; i < matrix.length; i++) {
        var filaPivote = matrix[FP];
        if (i != FP)
            matrix[i] = Opera(filaPivote, matrix[i], CP);
    }
}
// Retorna la fila pivote de la matriz a partir de la columna pivote
function getRowPivot(matrix, CP) {
    var sol = (matrix[1].length) - 1;
    var bucket = matrix[1][sol] / matrix[1][CP];
    var filPiv = 1;
    for (var i = 2; i < matrix.length; i++) {
        var collector = matrix[i][sol] / matrix[i][CP];
        if (collector < bucket && collector != 0) {
            bucket = collector;
            filPiv = i;
        }
    }
    return filPiv;
}

// Método simplex
function simplex(matrix) {

    var CP = getColPivot(matrix); //Columna pivote
    var FP = getRowPivot(matrix, CP); //Fila Pivote
    var PP = matrix[FP][CP];        //Punto Pivote
    // Variables de iteracion
    VarIterablesRow[FP] = VarIterablesCol[CP];
    // Realiza los Cálculos por Gauss Jorddan
    return Gauss(matrix, CP, FP, PP);
}

// Retorna la columna pivote de la matriz
function getColPivot(matrix) {
    var mostNegativeOfColumn = Math.min(...matrix[0]);
    return matrix[0].indexOf(mostNegativeOfColumn);
}


// Imprime la socucion del sistema arrojado por el método simplex
function printSolution(matrix) {
    // Cabecera de la tabla
    var padre = document.getElementById('sol_simplex');
    padre.removeChild(padre.firstElementChild);
    $("#sol_simplex").append("<table class='table table-hover' id='tabla_optima'></table>");
    $("#tabla_optima").append("<thead id='cabecera'></thead>");
    $("#cabecera").append("<th>Num Ecuaciones</th>");
    for (var i = 1; i <= variables; i++)
        $("#cabecera").append("<th>X" + i + "</th>");

    for (var i = 1; i <= restricciones; i++)
        $("#cabecera").append('<th class="sol">S' + i + "</th>");

    $("#cabecera").append("<th>Solución</th>");

    // contenido de la tabla con soluciiones
    var padre = document.getElementById('tabla_optima');
    var cuerpo = document.createElement("tbody");
    for (var i = 0; i < restricciones + 1; i++) {
        var fila = document.createElement("tr");
        var columna = document.createElement("th");
        var contenido = document.createTextNode(VarIterablesRow[i]);
        columna.appendChild(contenido);
        fila.appendChild(columna);
        cuerpo.appendChild(fila);
        for (var j = 0; j < restricciones + variables + 1; j++) {
            columna = document.createElement("td");
            contenido = document.createTextNode(matrix[i][j].toFixed(3));
            columna.appendChild(contenido);
            fila.appendChild(columna);
            cuerpo.appendChild(fila);
        }
    }
    padre.appendChild(cuerpo);

    var vs = matrix[0].length - 1; //indice del vector o columna de soluciones


    /*    for(let k =0; k<col1.length; k++) {
          var res = col1[k]*col2[k]
        }
        console.log(res1)
        console.log(col1)
        console.log(col2)
        console.log(mVA)*/


    for (var i = 0; i < VarIterablesRow.length; i++)
        console.log(VarIterablesRow[i] + " = " + matrix[i][vs]);
}


// Verifica si un valor de un input es vacio
function isEmpty(inputs) {
    var band = false;
    for (var i = 0; i < inputs.length; i++) {
        if (isNaN(inputs[i])) {
            band = true;
            break;
        }
    }
    return band;
}

// -----------VALIDA LOS INPUTS DE LA TABLA------------------------
function ValidaTabla() {
    var flag = true;
    for (var i = 0; i < restricciones + 1; i++) {
        if (isEmpty(extractValues(document.getElementsByName('variables_restriccion_' + i)))) {
            flag = false;
            break;
        }
    }
    return flag;
}


// function extractMatrix(a,b,matrix){
//     var cut = restricciones+1;
//     var B_1 = new Array();
//     var B_2 =  new Array();
//     B_1.push(matrix.slice(a,b+1));
//     for(var i=0; i<B_1.length; i++){
//         for(var j=0; j<restricciones; j++)
//     }
//     return B_2;
// }


// ---------------CAMBIOS EN EL VECTOR B-------------------------------

// function newSolution(matrix) {
//     var indexes = new Array();
//     var vectorB = extractValues(document.getElementsByName("new_VB"));
//     for(var i=0; i<VarIterablesRow.length; i++){
//         if (VarIterablesRow[i].startsWith("X"))
//             indexes.push(i);
//     }
//     var new_matrix = extractMatrix(indexes[0],indexes.pop(),matrix);
//     console.log(new_matrix);
// }


//multiplicar matrices
function multMVA(mVA, r, matrix) {
    var Ak = []
    var nuevo = []
    var m = $("#restriccion0 input").slice(0, r);
    //  var error = $("#error")
    $('#change_vectorA').click(function () {
        for (let i = 1; i < (r + 1); i++) {
            var a = $("#VA" + i).val();
            Ak.push(a)
        }
        Ak = Ak.filter(function (dato) {
            return dato != undefined
        });

        for (let j = 0; j < mVA.length; j++) {
            nuevo.push([Ak, mVA[j]])
        }
        suma = 0
        aux = []
        total = []
        for (let i = 0; i < (nuevo.length); i++) {
            var array = nuevo[i][1]
            var array1 = nuevo[i][0]
            for (let j = 0; j < array.length; j++) {
                suma = suma + array[j] * array1[j];
                aux.push(suma)
                if (aux.length == r) {
                    total.push(aux[aux.length - 1])
                    aux = []
                    suma = 0;
                }
            }
        }
        console.log(total)

        Ak = []
        for (let i = 0; i < m.length; i++) {
            var objetivo = m[i].value;
            Ak.push(objetivo)
        }

        var x = 0
        for (let i = 0; i < total.length; i++) {
            x = x + parseInt(Ak[i]) * total[i]
        }

        console.log(x)
        var ck = $("#CK").val();
        var result = parseInt(-ck) - x
        console.log(result)

        if (result > 0) {
            console.log("Es factible C: ")
        } else {
            mVA = [result]
            for (var i = 0; i < total.length; i++)
                mVA.push(total[i])

            for (let j = 0; j < matrix.length; j++)
                matrix[j].push(mVA[j])

            $("#resultado").innerHTML = "<h3>EL resultado Factible es: " + result +"</h3>";    
            var i = 0;
            while ((Math.min(...matrix[0])) < 0) {
                simplex(matrix);
                if (i >= 100)
                    break;
                i++
            }
            var vs = matrix[0].length - 1;
            for (var i = 0; i < VarIterablesRow.length; i++)
                console.log(VarIterablesRow[i] + " = " + matrix[i][vs]);

        }

    })
}
//Cambio en el vector a
function cambioVA(matrix) {
    var mVA = []
    var v = $("#variables").val();
    var r = $("#restricciones").val();

    for (let i = 1; i < matrix.length; i++) {
        var n = matrix[i].slice(v, -1)
        mVA.push(n)
    }
    console.log(mVA)
    multMVA(mVA, r, matrix);
}

// ------------------------FUNCIÍON PRINCIPAL------------------------
$('document').ready(function () {
    document.getElementById('generateTable').addEventListener("click", function () {
        VarIterablesCol = [];
        VarIterablesRow = [];
        VarIterablesRow.push("Z");
        restricciones = parseInt($('select[id=restricciones]').val());
        $('#calculate').removeAttr("disabled");
        variables = parseInt($('select[id=variables]').val());
        if (document.getElementById('datos').firstElementChild == null) {
            buildTable(variables, restricciones);
            buildChangeVector(restricciones);
        }
        else {
            document.getElementById('datos').firstElementChild.remove();
            $("input[id=VB]").remove('#VB');
            $("input[class=VA]").remove();
            buildTable(variables, restricciones);
            buildChangeVector(restricciones);
        }
    });

    // Botón con evento que hace el cálculo simplex
    document.getElementById('calculate').addEventListener('click', function () {
        if (ValidaTabla()) {
            var matrix = new Array();
            for (var i = 0; i < restricciones + 1; i++)
                matrix.push(extractValues(document.getElementsByName('variables_restriccion_' + i)));
            while ((Math.min(...matrix[0])) < 0)
                simplex(matrix);
            printSolution(matrix);
            cambioVA(matrix);


        } else {
            alert("!Aún hay campos vacios!");
        }
    }, false);



});
