$('document').ready(function(){
    calcular();
    $(".evento").keyup(calcular);
});

function calcular(){
    $("#Q, #ROP, #TC, #TY, #TW, #residuo").empty();
    var demanda = parseFloat($('input[id=lambda]').val());
    var costo_de_orden = parseFloat($('input[id=cto_orden]').val());
    var tau_weeks = parseFloat($('input[id=tau]').val());
    var tau_years = parseFloat($('input[id=tau_y]').val());
    var k = parseFloat($('input[id=CK]').val());
    var i = parseFloat($('input[id=CI]').val());
    $("#tau_y").val((tau_weeks/52).toFixed(3));
    tau_years = parseFloat($('input[id=tau_y]').val());    
    var Q = Math.sqrt((2*k*demanda)/(i*costo_de_orden));
    var tiempo_ciclo_N = demanda/Q;
    var T_years = 1/tiempo_ciclo_N;
    var T_weeks = T_years*52;
    var ROP;
    var residuo = tau_years%T_years;
    if(tau_years>T_years)
        ROP = T_years*demanda;             
    else
        ROP =  demanda*(residuo);
   
    $("#Q").append(Q.toFixed(3));    
    $("#TC").append(tiempo_ciclo_N.toFixed(3));
    $("#TY").append(T_years.toFixed(3));
    $("#TW").append(T_weeks.toFixed(3));
    $("#residuo").append(residuo.toFixed(3));
    $("#ROP").append(ROP.toFixed(3));
}